package com.mai.jwt.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mai.jwt.models.entities.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String>{
    UserEntity findByEmail(String email);

    UserEntity findByPhone(String phone);

    @Query(
        value = "SELECT * FROM users WHERE phone LIKE %:keyword% or email LIKE %:keyword%",
        countQuery = "SELECT count(*) FROM users WHERE phone LIKE %:keyword% or email LIKE %:keyword%",
        nativeQuery = true)
    Page<UserEntity> search(@Param("keyword") String keyword, Pageable pageable);
}