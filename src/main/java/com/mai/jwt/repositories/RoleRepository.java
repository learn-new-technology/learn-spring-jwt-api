package com.mai.jwt.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mai.jwt.models.entities.RoleEntity;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, String> {
    RoleEntity findByName(String name);

    RoleEntity findByCode(String code);

    @Query(
        value = "SELECT * FROM roles WHERE name LIKE %:keyword% or code LIKE %:keyword% or description LIKE %:keyword%",
        countQuery = "SELECT count(*) FROM roles WHERE name LIKE %:keyword% or code LIKE %:keyword% or description LIKE %:keyword%",
        nativeQuery = true)
    Page<RoleEntity> search(@Param("keyword") String keyword, Pageable pageable);

    @Query(
        value = "SELECT * FROM roles WHERE name LIKE %?1% or code LIKE %?1% or description LIKE %?1%",
        countQuery = "SELECT count(*) FROM roles WHERE name LIKE %?1% or code LIKE %?1% or description LIKE %?1%",
        nativeQuery = true)
    List<RoleEntity> searchByKeyword(String keyword);
}