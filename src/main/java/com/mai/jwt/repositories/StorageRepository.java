package com.mai.jwt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mai.jwt.models.entities.StorageEntity;

@Repository
public interface StorageRepository extends JpaRepository<StorageEntity, String>{
    StorageEntity findByName(String name);
}