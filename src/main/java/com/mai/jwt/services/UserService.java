package com.mai.jwt.services;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mai.jwt.models.entities.UserEntity;
import com.mai.jwt.repositories.UserRepository;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Cacheable(value = "users")
    public Page<UserEntity> findUsers(String keyword, Pageable pageable) {
        return userRepository.search(keyword, pageable);
    }

    @Cacheable(value = "user", key="#id")
    public String findById(String id) {
        return "Find one user By his id: " + id;
    }

    @Cacheable(value = "user", key="#email")
    public String findByEmail() {
        return "Find one user By his email";
    }

    @CachePut(value="user", key = "#user.id")
    public String save() {
        return "Save one user";
    }
    
    @CacheEvict(value="user", key="#id")
    public boolean deleteById(String id) {
        return true;
    }
}
