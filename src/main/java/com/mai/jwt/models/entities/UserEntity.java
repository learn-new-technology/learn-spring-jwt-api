package com.mai.jwt.models.entities;

// import java.util.HashSet;
// import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
// import javax.persistence.CascadeType;
// import javax.persistence.FetchType;
// import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.mai.jwt.models.entities.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@EqualsAndHashCode(callSuper = false)
public class UserEntity extends BaseEntity {
    
    @Column(name = "first_name", nullable = false, length = 75)
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 75)
    private String lastName;

    @Column(name = "gender", nullable = false, length = 10)
    private String gender;

    @Column(name = "phone", unique = true, nullable = false, length = 20)
    private String phone;

    @Column(name = "email", unique = true, nullable = false, length = 255, updatable = false)
    private String email;

    @Column(name = "password", length = 75)
    private String password;

    @Column(name = "activated", nullable = false)
    private boolean activated = false;

    // @OneToOne(cascade = CascadeType.REMOVE)
    // private StorageEntity avatar;

    // @ManyToMany(fetch = FetchType.EAGER)
    // private Set<RoleEntity> roleEntities = new HashSet<>();
}