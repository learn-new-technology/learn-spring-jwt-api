package com.mai.jwt.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.mai.jwt.models.entities.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "storages")
@EqualsAndHashCode(callSuper = false)
public class StorageEntity extends BaseEntity {
    
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Transient
    private String base64;

    @Column(name = "uri", unique = true, nullable = false)
    private String uri;

    @Column(name = "size", nullable = false)
    private long size;
}