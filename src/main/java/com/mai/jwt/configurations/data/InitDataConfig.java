package com.mai.jwt.configurations.data;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.mai.jwt.models.entities.UserEntity;
import com.mai.jwt.repositories.UserRepository;

@Configuration
public class InitDataConfig {
    
    @Bean
    CommandLineRunner initDataBase(Environment enviroment, UserRepository userRepository) {
        return args -> {
            System.out.println("Start of Laoding Default Datas");

            if (userRepository.findAll().isEmpty()) {
                userRepository.save(new UserEntity("Utilisateur", "USER", "MALE", "phone1", "user@email.com", "passwordEncoder.encode(\"test\")", true));
                userRepository.save(new UserEntity("Administrateur", "ADMIN", "FEMALE", "phone2", "admin@email.com", "passwordEncoder.encode(\"test\")", true));
            }
            
            System.out.println("End of Laoding Default Datas \n");
            System.out.println("The application name is " + enviroment.getProperty("spring.application.name"));
        };
    }
    
}
