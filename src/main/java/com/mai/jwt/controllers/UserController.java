package com.mai.jwt.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mai.jwt.models.entities.UserEntity;
import com.mai.jwt.services.UserService;
import com.mai.jwt.utils.Translator;

@RestController
@CrossOrigin(origins = { "*" }, maxAge = 3600)
@RequestMapping(value = "/api/v1/users")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public List<UserEntity> findUsers(@RequestParam(value = "keyword") String keyword, Pageable pageable) {
        logger.info("Find All Users");
        return userService.findUsers(keyword, pageable).getContent();
    }

    @GetMapping(value = "{id}", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public String findUserById(@PathVariable String id) {
        logger.info("Find User By Id.");

        if (id.equals("id")) {
            return Translator.toLocale("errors.user-not-found");
        }
        
        return userService.findById(id);
    }

    @PostMapping(value = "", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public String saveUser() {
        logger.info("Save User.");
        // System.out.println(userService.findByEmail());
        return userService.save();
    }

    @PutMapping(value = "{id}", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public String updateUser(@PathVariable String id) {
        logger.info("Update User.");
        // System.out.println(userService.findByEmail());

        if (id.equals("id")) {
            return Translator.toLocale("errors.user-not-found");
        }
        
        return userService.save();
    }

    @DeleteMapping(value = "{id}", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public String deleteUserById(@PathVariable String id) {
        logger.info("Delete User By Id.");
        
        if (id.equals("id")) {
            return Translator.toLocale("errors.user-not-found");
        }

        return (userService.deleteById(id)) ? Translator.toLocale("success.user-deleted") : Translator.toLocale("errors.user-not-found");
    }
}